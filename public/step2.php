<?php

declare(strict_types=1);

require 'common.php';

handle(
    new \App\Controller\RegistrationWizardStep2(
        new \App\Mutation\RegistrationWizardStep2(
            new \App\Service\RegistrationWizardSessionStorage(
                new \Symfony\Component\HttpFoundation\Session\Session,
                new \App\Service\RegistrationWizardSerializer,
            ),
        )
    )
);