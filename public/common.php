<?php

declare(strict_types=1);

require '../vendor/autoload.php';

function handle($controller): void
{
    $session = new \Symfony\Component\HttpFoundation\Session\Session();
    $request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
    $request->setSession($session);

    ($controller->handle($request))->send();
}
