<?php

declare(strict_types=1);

require '../vendor/autoload.php';

require 'common.php';

handle(
    new \App\Controller\RegistrationWizardStep1(
        new \App\Mutation\RegistrationWizardStep1(
            new \App\Service\RegistrationWizardSessionStorage(
                new \Symfony\Component\HttpFoundation\Session\Session,
                new \App\Service\RegistrationWizardSerializer,
            ),
        )
    )
);