<?php

declare(strict_types=1);

require 'common.php';

handle(
    new \App\Controller\RegistrationWizardForm(
        new \App\Service\RegistrationWizardSessionStorage(
            new \Symfony\Component\HttpFoundation\Session\Session,
            new \App\Service\RegistrationWizardSerializer
        ),
        new \Twig\Environment(new \Twig\Loader\FilesystemLoader(__DIR__ . '/../templates')),
    )
);