<?php

declare(strict_types=1);

require 'common.php';

$connection = \Doctrine\DBAL\DriverManager::getConnection(['url' => 'mysql://app:app@mariadb/app']);

handle(
    new \App\Controller\RegistrationWizardStep3(
        new \App\Repository\Customer($connection),
        new \App\Mutation\RegistrationWizardStep3(
            new \App\Service\RegistrationWizardSessionStorage(
                new \Symfony\Component\HttpFoundation\Session\Session,
                new \App\Service\RegistrationWizardSerializer
            ),
            new \App\Repository\Customer($connection),
            new \App\Service\PaymentDataStorage(\Symfony\Component\HttpClient\HttpClient::create()),
        ),
        new \Twig\Environment(new \Twig\Loader\FilesystemLoader(__DIR__ . '/../templates')),
    )
);