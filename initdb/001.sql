CREATE TABLE `customer`
(
    `uuid`            varchar(16)  NOT NULL DEFAULT '',
    `id`              int(11) unsigned NOT NULL AUTO_INCREMENT,
    `first_name`      varchar(255) NOT NULL DEFAULT '',
    `last_name`       varchar(255) NOT NULL DEFAULT '',
    `phone`           varchar(255) NOT NULL DEFAULT '',
    `street`          varchar(255) NOT NULL DEFAULT '',
    `address`         varchar(255) NOT NULL DEFAULT '',
    `house_number`    varchar(255) NOT NULL DEFAULT '',
    `zip`             varchar(255) NOT NULL DEFAULT '',
    `city`            varchar(255) NOT NULL DEFAULT '',
    `account_owner`   varchar(255) NOT NULL DEFAULT '',
    `iban`            varchar(255) NOT NULL DEFAULT '',
    `payment_data_id` varchar(255) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`),
    KEY               `uuid` (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4;