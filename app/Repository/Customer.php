<?php

declare(strict_types=1);

namespace App\Repository;

class Customer
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $connection;

    public function __construct(\Doctrine\DBAL\Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @throws \App\Exception\RegistrationWizardIncomplete()
     */
    public function register(string $uuid, \App\DTO\RegistrationWizard $registration): int
    {
        if (!$registration->getStep2() || !$registration->getStep3()) {
            throw new \App\Exception\RegistrationWizardIncomplete();
        }

        $this->connection->insert(
            'customer',
            [
                'uuid' => $uuid,
                'first_name' => $registration->getStep1()->getFirstName(),
                'last_name' => $registration->getStep1()->getLastName(),
                'phone' => $registration->getStep1()->getPhone(),
                'street' => $registration->getStep2()->getStreet(),
                'house_number' => $registration->getStep2()->getHouseNumber(),
                'zip' => $registration->getStep2()->getZip(),
                'city' => $registration->getStep2()->getCity(),
                'account_owner' => $registration->getStep3()->getOwner(),
                'iban' => $registration->getStep3()->getIban(),
            ]
        );

        return (int)$this->connection->lastInsertId();
    }

    /**
     * @throws \App\Exception\CustomerNotFound
     * @throws \App\Exception\CustomerAlreadyHasPaymentDataId
     */
    public function completeRegistrationWithPaymentDataId(string $uuid, string $paymentDataId): void
    {
        $row = $this->connection->fetchAssociative('SELECT * FROM customer WHERE uuid = ?', [$uuid]);
        if ($row === false) {
            throw new \App\Exception\CustomerNotFound;
        }

        if ($row['payment_data_id'] !== '') {
            throw new \App\Exception\CustomerAlreadyHasPaymentDataId;
        }

        $this->connection->update('customer', ['payment_data_id' => $paymentDataId], ['uuid' => $uuid]);
    }

    /**
     * @throws \App\Exception\CustomerNotFound
     */
    public function find(string $uuid): \App\DTO\Customer
    {
        $row = $this->connection->fetchAssociative('SELECT * FROM customer WHERE uuid = ?', [$uuid]);
        if ($row === false) {
            throw new \App\Exception\CustomerNotFound;
        }

        return $this->createDTO($row);
    }

    private function createDTO(array $row): \App\DTO\Customer
    {
        return new \App\DTO\Customer(
            $row['uuid'],
            $row['first_name'],
            $row['last_name'],
            $row['phone'],
            $row['street'],
            $row['house_number'],
            $row['zip'],
            $row['city'],
            $row['account_owner'],
            $row['iban'],
            (int)$row['id'],
            $row['payment_data_id']
        );
    }
}