<?php

declare(strict_types=1);

namespace App\Service;

/**
 * This could be skipped while making fast&dirty implementation - for example
 * by leveraging built-in session serialization, however every change in of the
 * wizard steps classes (for example adding extra step or field) might lead to inconsistency between
 * data stored in the session for the steps already filled in my the user and new definition
 * that was just deployed. Having this class in place will allow to manually handle such an edge cases
 * by added normalization and post-processing
 */
class RegistrationWizardSerializer
{
    public function toString(\App\DTO\RegistrationWizard $wizard): string
    {
        return serialize($wizard);
    }

    /**
     * @return \App\DTO\RegistrationWizard
     */
    public function fromString(string $wizard): \App\DTO\RegistrationWizard
    {
        $result = unserialize($wizard, ['allowed_classes' => true]);
        if ($result === false || !$result instanceof \App\DTO\RegistrationWizard) {
            throw new \App\Exception\RegistrationWizardUnserializeFailed();
        }
        return $result;
    }
}