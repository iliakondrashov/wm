<?php

declare(strict_types=1);

namespace App\Service;

class RegistrationWizardSessionStorage
{
    private const SESSION_KEY = 'registration_wizard';

    /**
     * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
     */
    private $session;

    /**
     * @var \App\Service\RegistrationWizardSerializer
     */
    private $serializer;

    public function __construct(
        \Symfony\Component\HttpFoundation\Session\SessionInterface $session,
        \App\Service\RegistrationWizardSerializer $serializer
    ) {
        $this->session = $session;
        $this->serializer = $serializer;
    }

    /**
     * @throws \App\Exception\RegistrationWizardHaventStarted
     * @throws \App\Exception\RegistrationWizardUnserializeFailed
     */
    public function read(): \App\DTO\RegistrationWizard
    {
        if (!$this->session->has(self::SESSION_KEY)) {
            throw new \App\Exception\RegistrationWizardHaventStarted();
        }

        try {
            return $this->serializer->fromString($this->session->get(self::SESSION_KEY));
        } catch (\App\Exception\RegistrationWizardUnserializeFailed $e) {
            // this is a very sensitive block - on dev&test environments we for sure want to show this exception
            // however on production we can't gracefully recover from it but we still do not want to show 5xx to the user
            // that's why I think it's reasonable here to use "check for production" anti-pattern:
            // if (isProd) {logEmergency(...); throw new \App\Exception\RegistrationWizardHaventStarted();} else { throw $e;}
            // so we will just enforce the user to go through registration wizard one more time from the beginning
            throw $e;
        }
    }

    public function write(\App\DTO\RegistrationWizard $wizard): void
    {
        $this->session->set(self::SESSION_KEY, $this->serializer->toString($wizard));
    }

    public function delete(): void
    {
        $this->session->remove(self::SESSION_KEY);
    }
}