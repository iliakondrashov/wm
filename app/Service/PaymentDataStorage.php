<?php

declare(strict_types=1);

namespace App\Service;

class PaymentDataStorage
{
    /**
     * @var \Symfony\Contracts\HttpClient\HttpClientInterface
     */
    private $httpClient;

    public function __construct(\Symfony\Contracts\HttpClient\HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @throws \App\Exception\PaymentDataStorageHttp
     * @throws \App\Exception\PaymentDataStorageResponseParsingFailed
     * @throws \App\Exception\PaymentDataStorageResponseUnexpected
     */
    public function storeCustomerDetails(int $customerId, string $iban, string $owner): string
    {
        try {
            $response = $this->httpClient->request(
                'POST',
                'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data',
                [
                    'json' => [
                        'customerId' => $customerId,
                        'iban' => $iban,
                        'owner' => $owner
                    ],
                ]
            );

            $result = json_decode($response->getContent(true), true, JSON_THROW_ON_ERROR);
        } catch (\Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface $e) {
            throw new \App\Exception\PaymentDataStorageHttp('', 0, $e);
        } catch (\JsonException $e) {
            throw new \App\Exception\PaymentDataStorageResponseParsingFailed('', 0, $e);
        }

        if (!isset($result['paymentDataId'])
            || empty($result['paymentDataId'])
            || !is_string($result['paymentDataId'])
        ) {
            throw new \App\Exception\PaymentDataStorageResponseUnexpected();
        }

        return $result['paymentDataId'];
    }
}