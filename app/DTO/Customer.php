<?php

declare(strict_types=1);

namespace App\DTO;

class Customer
{
    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $uuid;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $houseNumber;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $accountOwner;

    /**
     * @var string
     */
    private $iban;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $paymentDataId;

    public function __construct(
        string $uuid,
        string $firstName,
        string $lastName,
        string $phone,
        string $street,
        string $houseNumber,
        string $zip,
        string $city,
        string $accountOwner,
        string $iban,
        int $id,
        ?string $paymentDataId
    ) {
        $this->uuid = $uuid;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phone = $phone;
        $this->street = $street;
        $this->houseNumber = $houseNumber;
        $this->zip = $zip;
        $this->city = $city;
        $this->accountOwner = $accountOwner;
        $this->iban = $iban;
        $this->id = $id;
        $this->paymentDataId = $paymentDataId;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    public function getZip(): string
    {
        return $this->zip;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getAccountOwner(): string
    {
        return $this->accountOwner;
    }

    public function getIban(): string
    {
        return $this->iban;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPaymentDataId(): string
    {
        return $this->paymentDataId;
    }
}