<?php

declare(strict_types=1);

namespace App\DTO;

class RegistrationStep3
{
    /**
     * @var string
     */
    private $owner;

    /**
     * @var string
     */
    private $iban;

    public function __construct(string $owner, string $iban)
    {
        $this->owner = $owner;
        $this->iban = $iban;
    }

    public function getIban(): string
    {
        return $this->iban;
    }

    public function getOwner(): string
    {
        return $this->owner;
    }
}