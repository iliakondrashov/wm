<?php

declare(strict_types=1);

namespace App\DTO;

class RegistrationWizard
{
    /**
     * @var RegistrationStep1
     */
    private $step1;

    /**
     * @var RegistrationStep2|null
     */
    private $step2;

    /**
     * @var RegistrationStep3
     */
    private $step3;

    public function __construct(
        RegistrationStep1 $step1,
        ?RegistrationStep2 $step2 = null,
        ?RegistrationStep3 $step3 = null
    ) {
        if ($step3 && !$step2) {
            throw new \App\Exception\RegistrationsStepsInconsistent;
        }

        $this->step1 = $step1;
        $this->step2 = $step2;
        $this->step3 = $step3;
    }

    public function getStep1(): RegistrationStep1
    {
        return $this->step1;
    }

    public function getStep2(): ?RegistrationStep2
    {
        return $this->step2;
    }

    public function getStep3(): ?RegistrationStep3
    {
        return $this->step3;
    }
}