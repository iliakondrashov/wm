<?php

declare(strict_types=1);

namespace App\DTO;

class RegistrationStep2
{
    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $houseNumber;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $city;

    public function __construct(string $street, string $houseNumber, string $zip, string $city)
    {
        $this->street = $street;
        $this->houseNumber = $houseNumber;
        $this->zip = $zip;
        $this->city = $city;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    public function getZip(): string
    {
        return $this->zip;
    }
}