<?php

declare(strict_types=1);

namespace App\Mutation;

class RegistrationWizardStep3
{
    /**
     * @var \App\Service\RegistrationWizardSessionStorage
     */
    private $wizardStorage;

    /**
     * @var \App\Repository\Customer
     */
    private $customerRepository;

    /**
     * @var \App\Service\PaymentDataStorage
     */
    private $paymentDataStorage;

    public function __construct(
        \App\Service\RegistrationWizardSessionStorage $registrationWizardSessionStorage,
        \App\Repository\Customer $customerRepository,
        \App\Service\PaymentDataStorage $paymentDataStorage
    ) {
        $this->wizardStorage = $registrationWizardSessionStorage;
        $this->customerRepository = $customerRepository;
        $this->paymentDataStorage = $paymentDataStorage;
    }

    /**
     * @throws \App\Exception\Mutation\RegistrationWizardStepIncorrect
     * @throws \App\Exception\Mutation\PaymentDataStorageFailure
     */
    public function handle(string $newCustomerUuid, string $accountOwner, string $iban): void
    {
        try {
            $wizard = $this->wizardStorage->read();
        } catch (\App\Exception\RegistrationWizardHaventStarted $e) {
            throw new \App\Exception\Mutation\RegistrationWizardStepIncorrect('', 0, $e);
        }

        // ensure that wizard step is correct
        if (!$wizard->getStep2()) {
            // user haven't passed step 2
            throw new \App\Exception\Mutation\RegistrationWizardStepIncorrect;
        }

        $wizardNew = new \App\DTO\RegistrationWizard(
            $wizard->getStep1(),
            $wizard->getStep2(),
            new \App\DTO\RegistrationStep3(
                $accountOwner,
                $iban,
            )
        );

        $customerId = $this->customerRepository->register($newCustomerUuid, $wizardNew);

        try {
            $paymentDataId = $this->paymentDataStorage->storeCustomerDetails(
                $customerId,
                $iban,
                $accountOwner,
            );
        } catch (\App\Exception\PaymentDataStorage $e) {
            throw new \App\Exception\Mutation\PaymentDataStorageFailure('', 0, $e);
        }

        $this->customerRepository->completeRegistrationWithPaymentDataId($newCustomerUuid, $paymentDataId);

        $this->wizardStorage->delete();
    }
}