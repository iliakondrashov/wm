<?php

declare(strict_types=1);

namespace App\Mutation;

class RegistrationWizardStep2
{
    /**
     * @var \App\Service\RegistrationWizardSessionStorage
     */
    private $wizardStorage;

    public function __construct(
        \App\Service\RegistrationWizardSessionStorage $registrationWizardSessionStorage
    ) {
        $this->wizardStorage = $registrationWizardSessionStorage;
    }

    /**
     * @throws \App\Exception\Mutation\RegistrationWizardStepIncorrect
     */
    public function handle(string $street, string $houseNumber, string $zip, string $city): void
    {
        try {
            $wizard = $this->wizardStorage->read();
        } catch (\App\Exception\RegistrationWizardHaventStarted $e) {
            throw new \App\Exception\Mutation\RegistrationWizardStepIncorrect('', 0, $e);
        }

        // ensure that wizard step is correct
        if ($wizard->getStep2()) {
            // user already passed step 2
            throw new \App\Exception\Mutation\RegistrationWizardStepIncorrect;
        }

        $wizardNew = new \App\DTO\RegistrationWizard(
            $wizard->getStep1(),
            new \App\DTO\RegistrationStep2(
                $street,
                $houseNumber,
                $zip,
                $city,
            )
        );

        $this->wizardStorage->write($wizardNew);
    }
}