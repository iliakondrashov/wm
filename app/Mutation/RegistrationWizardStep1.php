<?php

declare(strict_types=1);

namespace App\Mutation;

class RegistrationWizardStep1
{
    /**
     * @var \App\Service\RegistrationWizardSessionStorage
     */
    private $wizardStorage;

    public function __construct(
        \App\Service\RegistrationWizardSessionStorage $registrationWizardSessionStorage
    ) {
        $this->wizardStorage = $registrationWizardSessionStorage;
    }

    /**
     * @throws \App\Exception\RegistrationWizardHaventStarted
     * @throws \App\Exception\Mutation\RegistrationWizardStepIncorrect
     */
    public function handle(string $firstName, string $lastName, string $phone): void
    {
        try {
            $this->wizardStorage->read();
            // user already passed step1
            throw new \App\Exception\Mutation\RegistrationWizardStepIncorrect;
        } catch (\App\Exception\RegistrationWizardHaventStarted $e) {
        }

        $wizard = new \App\DTO\RegistrationWizard(
            new \App\DTO\RegistrationStep1(
                $firstName,
                $lastName,
                $phone,
            )
        );

        $this->wizardStorage->write($wizard);
    }
}