<?php

declare(strict_types=1);

namespace App\Controller;

class RegistrationWizardStep1
{
    /**
     * @var \App\Mutation\RegistrationWizardStep1
     */
    private $mutation;

    public function __construct(\App\Mutation\RegistrationWizardStep1 $mutation)
    {
        $this->mutation = $mutation;
    }

    public function handle(\Symfony\Component\HttpFoundation\Request $request
    ): \Symfony\Component\HttpFoundation\Response {
        try {
            $this->mutation->handle(
                trim((string)$request->request->get('fistName')),
                trim((string)$request->request->get('lastName')),
                trim((string)$request->request->get('phone')),
            );
        } catch (\App\Exception\Mutation\RegistrationWizardStepIncorrect $e) {
            return new \Symfony\Component\HttpFoundation\RedirectResponse('/');
        }

        return new \Symfony\Component\HttpFoundation\RedirectResponse('/');
    }
}