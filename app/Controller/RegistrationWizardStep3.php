<?php

declare(strict_types=1);

namespace App\Controller;

class RegistrationWizardStep3
{
    /**
     * @var \App\Repository\Customer
     */
    private $customerRepository;

    /**
     * @var \App\Mutation\RegistrationWizardStep3
     */
    private $mutation;

    /**
     * @var \Twig\Environment
     */
    private $twig;

    public function __construct(
        \App\Repository\Customer $customerRepository,
        \App\Mutation\RegistrationWizardStep3 $mutation,
        \Twig\Environment $twig
    ) {
        $this->customerRepository = $customerRepository;
        $this->mutation = $mutation;
        $this->twig = $twig;
    }

    public function handle(\Symfony\Component\HttpFoundation\Request $request
    ): \Symfony\Component\HttpFoundation\Response {
        try {
            // https://github.com/ramsey/uuid can be used
            $uuid = uniqid();
            $this->mutation->handle(
                $uuid,
                trim($request->request->get('accountOwner', '')),
                trim($request->request->get('iban', ''))
            );
        } catch (\App\Exception\Mutation\RegistrationWizardStepIncorrect $e) {
            return new \Symfony\Component\HttpFoundation\RedirectResponse('/');
        } catch (\App\Exception\Mutation\PaymentDataStorageFailure $e) {
            return new \Symfony\Component\HttpFoundation\Response($this->twig->render("failure.twig"), 500);
        }

        $customer = $this->customerRepository->find($uuid);
        return new \Symfony\Component\HttpFoundation\Response(
            $this->twig->render("done.twig", ['customer' => $customer])
        );
    }
}