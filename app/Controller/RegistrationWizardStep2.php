<?php

declare(strict_types=1);

namespace App\Controller;

class RegistrationWizardStep2
{
    /**
     * @var \App\Mutation\RegistrationWizardStep2
     */
    private $mutation;

    public function __construct(\App\Mutation\RegistrationWizardStep2 $mutation)
    {
        $this->mutation = $mutation;
    }

    public function handle(\Symfony\Component\HttpFoundation\Request $request
    ): \Symfony\Component\HttpFoundation\Response {
        try {
            $this->mutation->handle(
                trim((string)$request->request->get('street')),
                trim((string)$request->request->get('houseNumber')),
                trim((string)$request->request->get('zip')),
                trim((string)$request->request->get('city')),
            );
        } catch (\App\Exception\Mutation\RegistrationWizardStepIncorrect $e) {
            return new \Symfony\Component\HttpFoundation\RedirectResponse('/');
        }

        return new \Symfony\Component\HttpFoundation\RedirectResponse('/');
    }
}