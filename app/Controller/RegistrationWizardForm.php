<?php

declare(strict_types=1);

namespace App\Controller;

class RegistrationWizardForm
{
    /**
     * @var \App\Service\RegistrationWizardSessionStorage
     */
    private $wizardStorage;

    /**
     * @var \Twig\Environment
     */
    private $twig;

    public function __construct(
        \App\Service\RegistrationWizardSessionStorage $registrationWizardSessionStorage,
        \Twig\Environment $twig
    ) {
        $this->wizardStorage = $registrationWizardSessionStorage;
        $this->twig = $twig;
    }

    public function handle(\Symfony\Component\HttpFoundation\Request $request
    ): \Symfony\Component\HttpFoundation\Response {
        try {
            $wizard = $this->wizardStorage->read();
            if ($wizard->getStep2()) {
                $step = 3;
            } elseif ($wizard->getStep1()) {
                $step = 2;
            }
        } catch (\App\Exception\RegistrationWizardHaventStarted $e) {
            $step = 1;
        }

        return new \Symfony\Component\HttpFoundation\Response($this->twig->render("step{$step}.twig"));
    }
}