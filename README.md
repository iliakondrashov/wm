# wm nda project

## to-do

* [x] core logic: implement blank DTO for registration wizard and 3 steps
* [x] add storage and api services, first tests
* [x] naive controllers
* [x] extract mutations from controllers
* [x] add fields to steps DTOs
* [x] migrate dev environment from `dev.sh` to docker-compose
* [x] add maraidb service and implement customer repository (make it use db instead of stubs)
* [x] implement API\PaymentDataStorage
* [x] add front-end - wizard pages, complete page
* [x] gracefully handle api failures on step3
* [x] from task description: explain why the code is structured the way it is
* [x] from task description: Describe possible performance optimizations for your Code.
* [x] from task description: Which things could be done better, than you�ve done it?

## How code is structured and why

I'm a big fan of having an application layer which represents product language,
or "domain" in terms of DDD. Here its done by classes in
`App\Mutation` and `App\Exception\Mutation` namespaces.

Small components are implemented as services, while mutations glue them together
and represent 1:1 user actions. Controllers are just a translation
layer between http request and mutation and do not contain any logic.

Services and repositories always return immutable data.

Some interesting edge cases were identified - I explained them in
the comments, please take a look at the following classes/methods:

* `\App\Service\RegistrationWizardSessionStorage::read`
* `\App\Service\RegistrationWizardSerializer`

## Possible performance optimizations

The slowest part is synchronous http request to payment data storage api. Also external apis
are not fully reliable, unless there is good retry policy is in place. I'd suggest to use queue
it to call asynchronously later on - this way we will solve both problems.

## Which things could be done better

* Instead of passing uuid as a string https://github.com/ramsey/uuid library can be used
* Any orm/ar library can be used in the repository instead of relatively low level dbal wrapper
* Laravel/Symfony/whatever framework can be used for http routing, error handling and DI
  instead of raw php scripts in `./public`
* End-to-end tests will be nice to have - currently controllers are not tested at all
* Use DB migration tool instead of just mounting `initdb` in the mariadb container;
  current approach also have a huge drawback - mariadb data volume have to be deleted in order
  new migrations to be applied, and this will mean loss of all existing data.

## What could be done differently

Transaction could be used to wrap db insert and api call, but I'm not a fan of doing that,
my main motivation is: it will break the isolation between two components: customer repository
and api wrapper. Distributed commits (or two phase commits) are complex to implement and maintain,
I'd rather go for simplicity and enforce each component (let's treat everything related to
modifying customer details as a component) to manage transactions on its own. It's not a hard opinion,
so to be evaluated in each particular case, but here is my motivation for it.

Extra field to reflect was registration completed or not could be added to the customer table,
currently, this can be checked by comparing paymentId with the empty string.

Nice and shiny front-end - I like back-end way more, so I put as little effort to html&css
as I could.