<?php

declare(strict_types=1);

namespace Test\Integration\Mutation;

class RegistrationWizardStep3Test extends \TestHelper\TestCaseFunctional
{
    public function testSuccess(): void
    {
        $registrationWizardSessionStorage = new \App\Service\RegistrationWizardSessionStorage(
            new \Symfony\Component\HttpFoundation\Session\Session(
                new \Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage()
            ),
            new \App\Service\RegistrationWizardSerializer
        );
        $mutation = new \App\Mutation\RegistrationWizardStep1($registrationWizardSessionStorage);
        $mutation->handle('ilia', 'k', '+31');

        $mutation = new \App\Mutation\RegistrationWizardStep2($registrationWizardSessionStorage);
        $mutation->handle('street', '13b', '1087KR', 'Amsterdam');

        $customerRepository = new \App\Repository\Customer($this->getConnection());
        $mutation = new \App\Mutation\RegistrationWizardStep3(
            $registrationWizardSessionStorage,
            $customerRepository,
            new \App\Service\PaymentDataStorage(\Symfony\Component\HttpClient\HttpClient::create())
        );
        $mutation->handle('newCustomerUuid', 'owner', 'iban');

        $customer = $customerRepository->find('newCustomerUuid');

        self::assertInstanceOf(\App\DTO\Customer::class, $customer);

        self::assertSame('ilia', $customer->getFirstName());
        self::assertSame('k', $customer->getLastName());
        self::assertSame('+31', $customer->getPhone());
        self::assertSame('street', $customer->getStreet());
        self::assertSame('13b', $customer->getHouseNumber());
        self::assertSame('1087KR', $customer->getZip());
        self::assertSame('Amsterdam', $customer->getCity());
        self::assertSame('owner', $customer->getAccountOwner());
        self::assertSame('iban', $customer->getIban());
        self::assertNotEmpty($customer->getPaymentDataId());
    }
}
