<?php

declare(strict_types=1);

namespace Test\Integration\Service;

use PHPUnit\Framework\TestCase;

class PaymentDataStorageTest extends TestCase
{
    public function testStoreCustomerDetails(): void
    {
        $api = new \App\Service\PaymentDataStorage(\Symfony\Component\HttpClient\HttpClient::create());
        $id = $api->storeCustomerDetails(10, 'NL85INGB123', 'owner');
        self::assertIsString($id);
        self::assertSame(96, strlen($id));
    }
}
