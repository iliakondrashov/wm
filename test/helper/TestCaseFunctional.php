<?php

declare(strict_types=1);

namespace TestHelper;

abstract class TestCaseFunctional extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $connection;

    protected function getConnection(): \Doctrine\DBAL\Connection
    {
        if (!$this->connection) {
            $this->connection = \Doctrine\DBAL\DriverManager::getConnection(['url' => 'mysql://app:app@mariadb/app']);
        }
        return $this->connection;
    }

    /**
     * @before
     */
    public function beginTransaction()
    {
        $this->getConnection()->beginTransaction();
    }

    /**
     * @after
     */
    public function rollback()
    {
        $this->getConnection()->rollBack();
    }
}
