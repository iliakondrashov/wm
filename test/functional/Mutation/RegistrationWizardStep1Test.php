<?php

declare(strict_types=1);

namespace Test\Functional\Mutation;

class RegistrationWizardStep1Test extends \PHPUnit\Framework\TestCase
{
    public function testSuccess(): void
    {
        $registrationWizardSessionStorage = new \App\Service\RegistrationWizardSessionStorage(
            new \Symfony\Component\HttpFoundation\Session\Session(
                new \Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage()
            ),
            new \App\Service\RegistrationWizardSerializer
        );
        $mutation = new \App\Mutation\RegistrationWizardStep1($registrationWizardSessionStorage);
        $mutation->handle('ilia', 'k', '+31');

        $wizard = $registrationWizardSessionStorage->read();

        self::assertNotNull($wizard->getStep1());
        self::assertNull($wizard->getStep2());
        self::assertNull($wizard->getStep3());

        self::assertSame('ilia', $wizard->getStep1()->getFirstName());
        self::assertSame('k', $wizard->getStep1()->getLastName());
        self::assertSame('+31', $wizard->getStep1()->getPhone());
    }
}
