<?php

declare(strict_types=1);

namespace Test\Functional\Mutation;

class RegistrationWizardStep2Test extends \PHPUnit\Framework\TestCase
{
    public function testSuccess(): void
    {
        $registrationWizardSessionStorage = new \App\Service\RegistrationWizardSessionStorage(
            new \Symfony\Component\HttpFoundation\Session\Session(
                new \Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage()
            ),
            new \App\Service\RegistrationWizardSerializer
        );
        $mutation = new \App\Mutation\RegistrationWizardStep1($registrationWizardSessionStorage);
        $mutation->handle('ilia', 'k', '+31');

        $mutation = new \App\Mutation\RegistrationWizardStep2($registrationWizardSessionStorage);
        $mutation->handle('street', '13b', '1087KR', 'Amsterdam');

        $wizard = $registrationWizardSessionStorage->read();

        self::assertNotNull($wizard->getStep1());
        self::assertNotNull($wizard->getStep2());
        self::assertNull($wizard->getStep3());

        self::assertSame('street', $wizard->getStep2()->getStreet());
        self::assertSame('13b', $wizard->getStep2()->getHouseNumber());
        self::assertSame('1087KR', $wizard->getStep2()->getZip());
        self::assertSame('Amsterdam', $wizard->getStep2()->getCity());
    }

    public function testWizardHaventStarted(): void
    {
        $registrationWizardSessionStorage = new \App\Service\RegistrationWizardSessionStorage(
            new \Symfony\Component\HttpFoundation\Session\Session(
                new \Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage()
            ),
            new \App\Service\RegistrationWizardSerializer
        );
        $mutation = new \App\Mutation\RegistrationWizardStep2($registrationWizardSessionStorage);

        $this->expectException(\App\Exception\Mutation\RegistrationWizardStepIncorrect::class);

        $mutation->handle('street', '13b', '1087KR', 'Amsterdam');
    }

    public function testStepAlreadyCompleted(): void
    {
        $registrationWizardSessionStorage = new \App\Service\RegistrationWizardSessionStorage(
            new \Symfony\Component\HttpFoundation\Session\Session(
                new \Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage()
            ),
            new \App\Service\RegistrationWizardSerializer
        );
        $mutation = new \App\Mutation\RegistrationWizardStep1($registrationWizardSessionStorage);
        $mutation->handle('ilia', 'k', '+31');

        $mutation = new \App\Mutation\RegistrationWizardStep2($registrationWizardSessionStorage);
        $mutation->handle('street', '13b', '1087KR', 'Amsterdam');

        $this->expectException(\App\Exception\Mutation\RegistrationWizardStepIncorrect::class);

        $mutation = new \App\Mutation\RegistrationWizardStep2($registrationWizardSessionStorage);
        $mutation->handle('street', '13b', '1087KR', 'Amsterdam');
    }
}
