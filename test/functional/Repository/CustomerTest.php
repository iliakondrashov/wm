<?php

declare(strict_types=1);

namespace Test\Functional\Repository;

class CustomerTest extends \TestHelper\TestCaseFunctional
{
    public function testRegister(): void
    {
        $wizard = new \App\DTO\RegistrationWizard(
            new \App\DTO\RegistrationStep1('f', 'l', 'p'),
            new \App\DTO\RegistrationStep2('s', 'h', 'z', 'a'),
            new \App\DTO\RegistrationStep3('a', 'b')
        );

        $repository = new \App\Repository\Customer($this->getConnection());
        $id = $repository->register('uuid', $wizard);
        self::assertIsInt($id);

        $customer = $repository->find('uuid');

        self::assertInstanceOf(\App\DTO\Customer::class, $customer);

        self::assertSame('f', $customer->getFirstName());
        self::assertSame('l', $customer->getLastName());
        self::assertSame('p', $customer->getPhone());
        self::assertSame('s', $customer->getStreet());
        self::assertSame('h', $customer->getHouseNumber());
        self::assertSame('z', $customer->getZip());
        self::assertSame('a', $customer->getCity());
        self::assertSame('a', $customer->getAccountOwner());
        self::assertSame('b', $customer->getIban());
    }

    public function testRegisterUniqueId(): void
    {
        $wizard = new \App\DTO\RegistrationWizard(
            new \App\DTO\RegistrationStep1('f', 'l', 'p'),
            new \App\DTO\RegistrationStep2('s', 'h', 'z', 'a'),
            new \App\DTO\RegistrationStep3('a', 'b')
        );

        $repository = new \App\Repository\Customer($this->getConnection());
        $id1 = $repository->register('uuid1', $wizard);
        $id2 = $repository->register('uuid2', $wizard);
        self::assertNotSame($id1, $id2);
    }

    public function testFindNotFound(): void
    {
        $repository = new \App\Repository\Customer($this->getConnection());
        $this->expectException(\App\Exception\CustomerNotFound::class);
        $repository->find('uuid');
    }

    public function testCompleteRegistrationWithPaymentDataId(): void
    {
        $wizard = new \App\DTO\RegistrationWizard(
            new \App\DTO\RegistrationStep1('f', 'l', 'p'),
            new \App\DTO\RegistrationStep2('s', 'h', 'z', 'a'),
            new \App\DTO\RegistrationStep3('a', 'b')
        );

        $repository = new \App\Repository\Customer($this->getConnection());
        $repository->register('uuid', $wizard);
        self::assertNull($repository->completeRegistrationWithPaymentDataId('uuid', 'paymentDataId'));
        self::assertSame('paymentDataId', $repository->find('uuid')->getPaymentDataId());
    }

    public function testCompleteRegistrationWithPaymentDataCustomerNotFound(): void
    {
        $repository = new \App\Repository\Customer($this->getConnection());
        $this->expectException(\App\Exception\CustomerNotFound::class);
        $repository->completeRegistrationWithPaymentDataId('uuid', 'id');
    }

    public function testCompleteRegistrationWithPaymentDataIdTwice(): void
    {
        $wizard = new \App\DTO\RegistrationWizard(
            new \App\DTO\RegistrationStep1('f', 'l', 'p'),
            new \App\DTO\RegistrationStep2('s', 'h', 'z', 'a'),
            new \App\DTO\RegistrationStep3('a', 'b')
        );

        $repository = new \App\Repository\Customer($this->getConnection());
        $repository->register('uuid', $wizard);
        $repository->completeRegistrationWithPaymentDataId('uuid', 'id');

        $this->expectException(\App\Exception\CustomerAlreadyHasPaymentDataId::class);

        $repository->completeRegistrationWithPaymentDataId('uuid', 'id');
    }
}
