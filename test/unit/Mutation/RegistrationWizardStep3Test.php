<?php

declare(strict_types=1);

namespace Test\Unit\Mutation;

class RegistrationWizardStep3Test extends \PHPUnit\Framework\TestCase
{
    public function testSuccess(): void
    {
        $registrationWizardSessionStorage = $this->createMock(
            \App\Service\RegistrationWizardSessionStorage::class
        );
        $customerRepository = $this->createMock(\App\Repository\Customer::class);
        $paymentDataStorage = $this->createMock(\App\Service\PaymentDataStorage::class);
        $mutation = new \App\Mutation\RegistrationWizardStep3(
            $registrationWizardSessionStorage,
            $customerRepository,
            $paymentDataStorage
        );

        $wizard = new \App\DTO\RegistrationWizard(
            new \App\DTO\RegistrationStep1('f', 'l', 'p'),
            new \App\DTO\RegistrationStep2('s', 'h', 'z', 'a'),
        );
        $registrationWizardSessionStorage->expects(self::once())->method('read')->willReturn($wizard);

        $customerRepository
            ->expects(self::once())
            ->method('register')
            ->with(
                'newCustomerUuid',
                new \App\DTO\RegistrationWizard(
                    $wizard->getStep1(),
                    $wizard->getStep2(),
                    new \App\DTO\RegistrationStep3('owner', 'iban'),
                )
            )
            ->willReturn(10);

        $paymentDataStorage
            ->expects(self::once())
            ->method('storeCustomerDetails')
            ->with(10, 'iban', 'owner')
            ->willReturn('paymentDataUuid');

        $customerRepository
            ->expects(self::once())
            ->method('completeRegistrationWithPaymentDataId')
            ->with('newCustomerUuid', 'paymentDataUuid');

        $mutation->handle('newCustomerUuid', 'owner', 'iban');
    }

    public function testPaymentDataStorageFailure(): void
    {
        $registrationWizardSessionStorage = $this->createMock(
            \App\Service\RegistrationWizardSessionStorage::class
        );
        $customerRepository = $this->createMock(\App\Repository\Customer::class);
        $paymentDataStorage = $this->createMock(\App\Service\PaymentDataStorage::class);
        $mutation = new \App\Mutation\RegistrationWizardStep3(
            $registrationWizardSessionStorage,
            $customerRepository,
            $paymentDataStorage
        );

        $wizard = new \App\DTO\RegistrationWizard(
            new \App\DTO\RegistrationStep1('f', 'l', 'p'),
            new \App\DTO\RegistrationStep2('s', 'h', 'z', 'a'),
        );
        $registrationWizardSessionStorage->expects(self::once())->method('read')->willReturn($wizard);

        $customerRepository
            ->expects(self::once())
            ->method('register')
            ->with(
                'newCustomerUuid',
                new \App\DTO\RegistrationWizard(
                    $wizard->getStep1(),
                    $wizard->getStep2(),
                    new \App\DTO\RegistrationStep3('owner', 'iban'),
                )
            )
            ->willReturn(10);

        $paymentDataStorage
            ->expects(self::once())
            ->method('storeCustomerDetails')
            ->with(10, 'iban', 'owner')
            ->willThrowException(new \App\Exception\PaymentDataStorage);

        $customerRepository
            ->expects(self::never())
            ->method('completeRegistrationWithPaymentDataId');

        $this->expectException(\App\Exception\Mutation\PaymentDataStorageFailure::class);

        $mutation->handle('newCustomerUuid', 'owner', 'iban');
    }
}
