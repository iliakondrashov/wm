<?php

declare(strict_types=1);

namespace Test\Unit\Service;

use PHPUnit\Framework\TestCase;

class RegistrationWizardSerializerTest extends TestCase
{
    public function testSerializeUnserialize(): void
    {
        $step1 = new \App\DTO\RegistrationStep1('f', 'l', 'p');
        $wizard = new \App\DTO\RegistrationWizard($step1);
        $service = new \App\Service\RegistrationWizardSerializer();
        $string = ($service)->toString($wizard);
        self::assertIsString($string);
        $wizard2 = $service->fromString($string);
        self::assertNotSame($wizard, $wizard2);
        self::assertEquals($wizard, $wizard2);
    }

    public function testUnserializeFailed1(): void
    {
        $this->expectException(\App\Exception\RegistrationWizardUnserializeFailed::class);
        $service = new \App\Service\RegistrationWizardSerializer();
        $service->fromString(serialize(new \stdClass()));
    }

    public function testUnserializeFailed2(): void
    {
        $this->expectException(\App\Exception\RegistrationWizardUnserializeFailed::class);
        $service = new \App\Service\RegistrationWizardSerializer();
        $service->fromString('bla');
    }
}
